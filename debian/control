Source: jsoncpp
Section: libs
Priority: optional
Maintainer: Mairi Dubik <mairi.dubik@clevon.com>
Homepage: https://github.com/open-source-parsers/jsoncpp
Vcs-Browser: https://gitlab.com/clevon/debian/libjsoncpp
Vcs-Git: https://gitlab.com/clevon/debian/libjsoncpp.git
Rules-Requires-Root: no
Standards-Version: 4.7.0
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 python3:native <!nocheck>,
Build-Depends-Indep:
 doxygen <!nodoc>,

Package: libjsoncpp25
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C++ library for interacting with JSON
 JsonCpp is a C++ library that allows manipulating JSON values, including
 serialization and deserialization to and from strings. It can also preserve
 existing comment in unserialization/serialization steps, making it a
 convenient format to store user input files.

Package: libjsoncpp-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libjsoncpp25 (= ${binary:Version}), ${misc:Depends}
Description: C++ library for interacting with JSON (development files)
 JsonCpp is a C++ library that allows manipulating JSON values, including
 serialization and deserialization to and from strings. It can also preserve
 existing comment in unserialization/serialization steps, making it a
 convenient format to store user input files.

Package: libjsoncpp-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends: ${misc:Depends}
Suggests: libjsoncpp-dev
Description: C++ library for interacting with JSON (API documentation)
 JsonCpp is a C++ library that allows manipulating JSON values, including
 serialization and deserialization to and from strings. It can also preserve
 existing comment in unserialization/serialization steps, making it a
 convenient format to store user input files.
